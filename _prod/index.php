<?php

//ob_start();
// if(isset($_GET["session_id"])) {
	// session_id($_GET["session_id"]);
// }
//session_start();

require_once '_shared.php';

runApi();

function parseAndGetParams() {
	// get the original query string
	$params = !empty($_GET['params']) ? $_GET['params'] : false;
	
	// if there are no params, set to false and return
	if(empty($params)) {
			return false;
	}

	// append '/' if none found
	if(strrpos($params, '/') === false) $params .= '/';
	$params = explode('/', $params);
	
	// take out the empty element at the end
	if(empty($params[count($params) - 1])) array_pop($params);
	
	return $params;
}

function runApi () {
	/*
	* 1st param: action
	* 2nd param: detailed action
	* 3rd param: props
	*/
	
	$end_point = parseAndGetParams();
	
	$http_method = $request_method=$_SERVER["REQUEST_METHOD"];
	$action = isset($end_point[0]) ? $end_point[0] : "";
	$detailed_action = isset($end_point[1]) ? $end_point[1] : "";
	$prop = isset($end_point[2]) ? $end_point[2] : "";
	
	switch ($action) {
		
		case "json"://to excel / csv
			require_once 'json.php';
			if( runDetailedAction($detailed_action) ) { runJson($http_method, $detailed_action, $prop); }
			break;
			
		case "xml"://to excel / csv
			require_once 'xml.php';
			if( runDetailedAction($detailed_action) ) { runXml($http_method, $detailed_action, $prop); }
			break;
			
		case "excel"://to json / xml
			require_once 'excel.php';
			if( runDetailedAction($detailed_action) ) { runExcel($http_method, $detailed_action, $prop); }
			break;
			
		case "csv"://to json / xml
			require_once 'csv.php';
			if( runDetailedAction($detailed_action) ) { runCsv($http_method, $detailed_action, $prop); }
			break;
			
		default:
			header_status(404);
	}
	
}

function runDetailedAction ($detailed_action) {
	if( strlen($detailed_action) > 0 ) {
		return true;
	} else {
		header_status(404);
		return false;
	}
}

?>