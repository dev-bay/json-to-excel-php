<?php

/////////////////
// MODELS LIST
/////////////////

require_once '_shared.php';
require_once 'phpspreadsheet/autoload.php';
require_once '_shared_create_excel_methods.php';

function runJson ($http_method, $detailed_action, $prop = "") {
	$action = $detailed_action;
	$data = $_POST['data'];
	
	run_action($http_method, $action, $prop, $data);
}

function run_action ($http_method, $action, $prop, $data) {
	switch($http_method) {
		case "POST":
			run_POST($action, $prop, $data);
			break;
	}

}

function run_POST ($action, $prop, $data) {
	switch($action) {
		case "flatify":
			if( checkAppKeys((float)$_POST["uploadSizeAscii"], (float)$_POST["uploadSizeHex"], (float)$_POST["appKey"], $_POST['recaptchaResponse']) ) {
				$display_data = flatify($data);
			
				if($display_data) {
					show_request($display_data);
				} else {
					show_request($display_data, 404);
				}
			} else {
				show_request("", 401);
			}
			
			break;
		case "excel":
			if( checkAppKeys((float)$_POST["uploadSizeAscii"], (float)$_POST["uploadSizeHex"], (float)$_POST["appKey"], $_POST['recaptchaResponse']) ) {
				convert_by_php_spreadsheet($data, 'excel', $prop);
			} else {
				show_request("", 401);
			}
			break;
		case "csv":
			if( checkAppKeys((float)$_POST["uploadSizeAscii"], (float)$_POST["uploadSizeHex"], (float)$_POST["appKey"], $_POST['recaptchaResponse']) ) {
				convert_by_php_spreadsheet($data, 'csv', $prop);
			} else {
				show_request("", 401);
			}
			break;
	}
}

function flatify ($data_pure = "") {	
	if(!is_json($data_pure)) {
		return false;
	}
	
	try {
		$obj = postRestAPI(helpersApi() . "/json-flatify", $data_pure, array('Content-Type: application/json; charset=utf-8', 'app_key: ' . get_nodejs_api_key()));
	} catch (Exception $e) {
		return false;
	}
	
	if(!is_json($obj)) {
		return false;
	}
	
	return json_decode($obj);
}









?>