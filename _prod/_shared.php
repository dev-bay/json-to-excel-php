<?php

require_once '_config_prod.php';

function cors() {

	// Access-Control headers are received during OPTIONS requests
	
		header('Access-Control-Allow-Origin: *');
		
		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				// may also be using PUT, PATCH, HEAD etc
			header("Access-Control-Allow-Methods: POST, OPTIONS");       

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
			header("Access-Control-Allow-Headers: content-type, x-xsrf-token, Origin, X-Auth-Token");
		
		
}

function is_json ($string) {
	json_decode($string);
	return (json_last_error() == JSON_ERROR_NONE);
}

function postRestAPI ($url, $curl_post_data, $curl_post_headers = null) {
	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($curl, CURLOPT_TIMEOUT, 50);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($curl, CURLOPT_FAILONERROR, true);
	
	if(isset($curl_post_headers)) {
		curl_setopt($curl, CURLOPT_HTTPHEADER, $curl_post_headers);
	} else {
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: text/plain'));
	}

	$result = curl_exec($curl);
	if(curl_errno($curl)) {
		return false;
	}

	curl_close($curl);

	return $result;
}

function getRestAPI ($url, $curl_post_headers = null) {
	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($curl, CURLOPT_TIMEOUT, 120);
	
	if(isset($curl_post_headers)) {
		curl_setopt($curl, CURLOPT_HTTPHEADER, $curl_post_headers);
	}

	$result = curl_exec($curl);
	if(curl_errno($curl)) {
		header_status(404);
		echo '{"status": 404, "message": "Connection error"}';
	}

	curl_close($curl);

	return $result;
}

function return_error ($msg = "", $code = 415) {
	echo $msg ? $msg : "Something went wrong. Check your file and try again later.";
	header_status($code ? $code : 415);
}

function generateRandomString($length = 10) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}

function show_request ($display_data, $status = null, $numeric_check = true) {
	if($display_data) {
		header_status($status ? $status : 200);
		
		if($numeric_check) {
			echo json_encode($display_data, JSON_NUMERIC_CHECK);
		} else {
			echo json_encode($display_data);
		}
		
	} else {
		header_status($status ? $status : 204); //na test/prod ma być tu 204!!!
	}
}

function checkAppKeys ($uploadSizeAscii, $uploadSizeHex, $appKey, $recaptchaResponse) {
	// Build POST request:
	$recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
	$recaptcha_secret = '6LfwCR8aAAAAAEHRYPShbUMoEGMTn4-J3OD-Hrru';
	$recaptcha_response = $recaptchaResponse;

	// Make and decode POST request:
	$recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
	$recaptcha = json_decode($recaptcha);
	
	if (!isset($recaptcha->score) || $recaptcha->score < 0.5) { // Take action based on the score returned:
		return false;
	}
	
	if($uploadSizeAscii === 0 || $uploadSizeHex === 0 || $appKey === 0 ) {
		return false;
	} else {
		$uploadSizeAscii = is_numeric($uploadSizeAscii) ? $uploadSizeAscii : 1;
		$uploadSizeHex = is_numeric($uploadSizeHex) ? $uploadSizeHex : 1;
		$appKey = is_numeric($appKey) ? $appKey : 1;
	}
	
	$data = [
		"uploadSizeAscii" => $uploadSizeAscii,
		"uploadSizeHex" => $uploadSizeHex,
		"appKey" => $appKey
	];
	
	try {
		$res = postRestAPI(helpersApi() . "/validate-keys", json_encode($data), array('Content-Type: application/json; charset=utf-8', 'app_key: ' . get_nodejs_api_key()));
		return $res === 'ok' ? true : false;
	} catch (Exception $e) {
		return_error("Connection error. Try again later.", 400);
		return false;
	}
}

//////////////////////////
// SERWER STATUS MESSAGES
/////////////////////////
function header_status($statusCode) {
	static $status_codes = null;

	if ($status_codes === null) {
			$status_codes = array (
					100 => 'Continue',
					101 => 'Switching Protocols',
					102 => 'Processing',
					200 => 'OK',
					201 => 'Created',
					202 => 'Accepted',
					203 => 'Non-Authoritative Information',
					204 => 'No Content',
					205 => 'Reset Content',
					206 => 'Partial Content',
					207 => 'Multi-Status',
					300 => 'Multiple Choices',
					301 => 'Moved Permanently',
					302 => 'Found',
					303 => 'See Other',
					304 => 'Not Modified',
					305 => 'Use Proxy',
					307 => 'Temporary Redirect',
					400 => 'Bad Request',
					401 => 'Unauthorized',
					402 => 'Payment Required',
					403 => 'Forbidden',
					404 => 'Not Found',
					405 => 'Method Not Allowed',
					406 => 'Not Acceptable',
					407 => 'Proxy Authentication Required',
					408 => 'Request Timeout',
					409 => 'Conflict',
					410 => 'Gone',
					411 => 'Length Required',
					412 => 'Precondition Failed',
					413 => 'Request Entity Too Large',
					414 => 'Request-URI Too Long',
					415 => 'Unsupported Media Type',
					416 => 'Requested Range Not Satisfiable',
					417 => 'Expectation Failed',
					422 => 'Unprocessable Entity',
					423 => 'Locked',
					424 => 'Failed Dependency',
					426 => 'Upgrade Required',
					500 => 'Internal Server Error',
					501 => 'Not Implemented',
					502 => 'Bad Gateway',
					503 => 'Service Unavailable',
					504 => 'Gateway Timeout',
					505 => 'HTTP Version Not Supported',
					506 => 'Variant Also Negotiates',
					507 => 'Insufficient Storage',
					509 => 'Bandwidth Limit Exceeded',
					510 => 'Not Extended'
			);
	}

	if ($status_codes[$statusCode] !== null) {
			$status_string = $statusCode . ' ' . $status_codes[$statusCode];
			header($_SERVER['SERVER_PROTOCOL'] . ' ' . $status_string, true, $statusCode);
	}
}

?>