<?php

/////////////////
// MODELS LIST
/////////////////

require_once '_shared.php';
require_once 'phpspreadsheet/autoload.php';
require_once '_shared_read_excel_methods.php';

function runCsv ($http_method, $detailed_action, $prop = "") {
	$action = $detailed_action;
	$data = $_FILES;
	
	run_action($http_method, $action, $prop, $data);
}

function run_action ($http_method, $action, $prop, $data) {
	switch($http_method) {
		case "POST":
			run_POST($action, $prop, $data);
			break;
	}

}

function run_POST ($action, $prop, $data) {
	switch($action) {
		case "json":
			
			if( checkAppKeys((float)$_POST["uploadSizeAscii"], (float)$_POST["uploadSizeHex"], (float)$_POST["appKey"], $_POST['recaptchaResponse']) ) {
				$rows_data = json_decode(base64_decode($prop), true);
				$res = make_json($rows_data);
			} else {
				show_request("", 401);
			}
			
			break;
		case "xml":
			
			if( checkAppKeys((float)$_POST["uploadSizeAscii"], (float)$_POST["uploadSizeHex"], (float)$_POST["appKey"], $_POST['recaptchaResponse']) ) {
				$rows_data = json_decode(base64_decode($prop), true);
				$res = make_xml($rows_data);
			} else {
				show_request("", 401);
			}
			
			break;
	}
}









?>