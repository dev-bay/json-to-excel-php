<?php

/////////////////
// MODELS LIST
/////////////////

require_once '_shared.php';
require_once 'phpspreadsheet/autoload.php';
require_once '_shared_create_excel_methods.php';

function runXml ($http_method, $detailed_action, $prop = "") {
	$action = $detailed_action;
	$data = $_POST['data'];
	
	run_action($http_method, $action, $prop, $data);
}

function run_action ($http_method, $action, $prop, $data) {
	switch($http_method) {
		case "POST":
			run_POST($action, $prop, $data);
			break;
	}

}

function run_POST ($action, $prop, $data) {
	switch($action) {
		case "json-from-xml":
			echo xml_to_json($data);
			break;
		case "excel":
			if( checkAppKeys((float)$_POST["uploadSizeAscii"], (float)$_POST["uploadSizeHex"], (float)$_POST["appKey"], $_POST['recaptchaResponse']) ) {
				$json = xml_to_json($data);
				$json_formatted = format_json($json);
				convert_by_php_spreadsheet($json_formatted, 'excel', $prop);
			} else {
				show_request("", 401);
			}
			
			break;
		case "csv":
			if( checkAppKeys((float)$_POST["uploadSizeAscii"], (float)$_POST["uploadSizeHex"], (float)$_POST["appKey"], $_POST['recaptchaResponse']) ) {
				$json = xml_to_json($data);
				$json_formatted = format_json($json);
				convert_by_php_spreadsheet($json_formatted, 'csv', $prop);
			} else {
				show_request("", 401);
			}
			break;
	}
}

function xml_to_json ($xml) {
	$xml = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_PARSEHUGE);
	$json2 = json_encode($xml);
	
	return $json2;
}

function format_json ($json_inn) {
	$helpers_api = helpersApi();
	$obj = postRestAPI($helpers_api . "/format-json-from-xml", $json_inn, array('Content-Type: application/json; charset=utf-8', 'app_key: ' . get_nodejs_api_key() ));
	
	return $obj;
}











?>