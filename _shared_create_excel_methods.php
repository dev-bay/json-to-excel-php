<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Ods;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

function convert_by_php_spreadsheet ($data_pure = "", $format, $prop) {	
	if(!is_json($data_pure)) {
		return_error();
		return false;
	}
	
	try {
		$obj = postRestAPI(helpersApi() . "/format-json-to-excel", $data_pure, array('Content-Type: application/json; charset=utf-8', 'app_key: ' . get_nodejs_api_key() ));
	} catch (Exception $e) {
		return_error("Connection error. Try again later.", 400);
		return false;
	}
	
	if(!is_json($obj)) {
		return_error();
		return false;
	}
	
	try {
		create_excel($obj, $format, $prop);
	} catch (Exception $e) {
		return_error();
		return false;
	}
	
}

function create_excel ($obj, $format, $prop) {
	$obj = json_decode($obj, true);
	
	$sheets = is_array($obj) && isset($obj["sheets"]) ? $obj["sheets"] : null;
	$columns = is_array($obj) && isset($obj["columns"]) && is_array($obj["columns"]) ? $obj["columns"] : null;
	$data = is_array($obj) && isset($obj["data"]) && is_array($obj["data"]) ? $obj["data"] : [];
	
	$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
	
	try {
		insert_data_into_excel($spreadsheet, $sheets, $columns, $data, $format, $prop);
	} catch (Exception $e) {
		return_error();
		return false;
	}
	
	$spreadsheet->disconnectWorksheets();
	unset($spreadsheet);
}

function insert_data_into_excel ($spreadsheet, $sheets, $columns, $data, $format, $prop = null) {
	if(isset($sheets) && $sheets && count($sheets)) {
		$i = 0;
		foreach($sheets as $key => $sheetArr) {
			if($i !== 0) {
				$spreadsheet->createSheet($i);
			}
			
			$spreadsheet->setActiveSheetIndex($i);
			$sheet = $spreadsheet->getActiveSheet();
			$sheet->setTitle($key);
			$sheet = insertSheetData($sheetArr["columns"], $sheetArr["data"], $sheet);
			
			$i++;
		}
	} else {
		$spreadsheet->setActiveSheetIndex(0);
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle("MAIN");
		$sheet = insertSheetData($columns, $data, $sheet);
	}
	
	switch ($format) {
		case "excel":
			$extension = $prop;
			
			if($extension === "xlsx") {
				$writer = new Xlsx($spreadsheet);
				header('content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('content-disposition: attachment; filename="json-to-excel-(dev-bay.com).xlsx"');
			} else if($extension === "ods") {
				$writer = new Ods($spreadsheet);
				header('content-type: application/vnd.oasis.opendocument.spreadsheet');
				header('content-disposition: attachment; filename="json-to-excel-(dev-bay.com).ods"');
			} else {
				$writer = new Xls($spreadsheet);
				header('content-type: application/vnd.ms-excel');
				header('content-disposition: attachment; filename="json-to-excel-(dev-bay.com).xls"');
			}
			
			
			break;
		case "csv":
			$options = json_decode(base64_decode($prop), true);
			$writer = new Csv($spreadsheet);
			
			$writer->setDelimiter($options['delimiter']);
			$writer->setEnclosure($options['enclosure']);
			$writer->setLineEnding($options['lineEnding']);
			
			header('Content-Type: application/csv');
			header('content-disposition: attachment; filename="json-to-excel-(dev-bay.com).csv"');
			
			break;
			
	}
	
	$writer->save("php://output");
}

function insertSheetData ($columns, $data, $sheet) {
	//DATA
	if($columns) {
		foreach($columns as $key => $col) {
			$sheet->setCellValueByColumnAndRow($key + 1, 1, $col);
			$col_letter = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($key + 1);
			$sheet->getColumnDimension($col_letter)->setAutoSize(true);
		}
	}
	
	$sheet->fromArray($data, NULL, $columns ? 'A2' : 'A1');

	//STYLES
	$header_styles = [
		'alignment' => [
			'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
		],
		'font' => [
			'bold' => true,
			'color' => ['rgb' => '000000'],
			'size' => 12
		],
		'fill' => [
			'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
			'color' => [ 'rgb' => 'aebcc5' ]
		]
	];
	
	$highest_column = $sheet->getHighestColumn();
	$sheet->calculateColumnWidths();
	
	if($columns) {
		$sheet->getStyle("A1:{$highest_column}1")->applyFromArray($header_styles);
	}
	
	return $sheet;
}

?>