<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

function make_json ($rows_data) {
	$file = get_files(get_upload_directory());
	
	if(isset($file["status"]) && $file["status"] === true) {
		try {
			$data = import_xls($file["file"], get_upload_directory(), $rows_data);
			unlink_files([$file], get_upload_directory());
		
			if($data) {
				header('Content-disposition: attachment; filename=excel-to-json-(dev-bay.com).json');
				header('Content-type: application/json');
				echo json_encode($data['data']);
				
				return $data;
			} else {
				return_error();
				return false;
			}
			
		} catch (Exception $e) {
			unlink_files([$file], get_upload_directory());
			return_error();
			return false;
		}
	} else {
		return_error();
		return false;
	}
}

function make_xml ($rows_data) {
	$file = get_files(get_upload_directory());
	
	if(isset($file["status"]) && $file["status"]) {
		try {
			$data = import_xls($file["file"], get_upload_directory(), $rows_data, true);
			unlink_files([$file], get_upload_directory());
			
			if($data) {
				$data_res = $data['data'];
				$xml_data = new SimpleXMLElement('<?xml version="1.0"?><data></data>');
				array_to_xml($data_res, $xml_data);
			
				header('Content-type: text/xml');
				header('Content-Disposition: attachment; filename="excel-to-xml-(dev-bay.com).xml"');
				print $xml_data->asXML();
				
				return $data;
			} else {
				return_error();
				return false;
			}
		} catch (Exception $e) {
			unlink_files([$file], get_upload_directory());
			return_error();
			return false;
		}
		
	} else {
		return_error();
		return false;
	}
}

function validate_xml_name ($name) {
	try {
		new DOMElement($name);
		return $name;
	} catch (DOMException $e) {
		$name = preg_replace("/[^a-zA-Z]/", "", strtolower($name));
		
		if(strlen($name) !== 0) {
			if(substr( $name, 0, 3 ) === "xml") {
				$name = str_replace("xml", "", $name);
			}
			
			return $name;
		} else {
			return "empty-header";
		}
		
		return false;
	}
}

function array_to_xml($data,  & $xml_data) {
  foreach($data as $key => $value) {
    if (is_array($value)) {
      if (is_numeric($key)) {
        $key = 'item'.$key; //dealing with <0/>..<n/> issues
      }
      $subnode = $xml_data->addChild($key);
      array_to_xml($value, $subnode);
    } else {
      $xml_data->addChild("$key", htmlspecialchars("$value"));
    }
  }
}

function import_xls ($target_file = "", $directory = null, $rows_data, $for_xml = false) {
	$directory = $directory ? $directory : './edR8gdJmIS/';
	$target_file_real_ext = str_replace(".xyz123", "", $target_file);
	
	rename($directory . $target_file, $directory . $target_file_real_ext); //protection name is changed to real extensin just before preocessing by php spreadsheet, and just after that, it is renamed backword
	
	try {
		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($directory . $target_file_real_ext);
		rename($directory . $target_file_real_ext, $directory . $target_file);
	} catch (Exception $e) {
		rename($directory . $target_file_real_ext, $directory . $target_file);
		return false;
	}
	
	$spreadsheet->setActiveSheetIndex(0);
	$sheetCount = $spreadsheet->getSheetCount();
	$res = [];
	
	if($sheetCount === 1) {
		
		$sheet = $spreadsheet->getActiveSheet();
		$sheet_data = get_sheet_data($rows_data, $sheet, $for_xml);
		
		$res = [
			'type' => 'single',
			'data' => $sheet_data["data"]
		];
		
	} else if ($sheetCount > 1){
		
		$sheet_names = $spreadsheet->getSheetNames();
		$res_data = [];
		
		foreach($sheet_names as $sheet_name) {
			$sheet = $spreadsheet->getSheetByName($sheet_name);
			
			array_push($res_data, get_sheet_data($rows_data, $sheet, $for_xml, $sheet_name));
			
			$res = [
				'type' => 'multi',
				'data' => $res_data
			];
		}
		
	} else {
		
		$res = false;
	}
	
	$spreadsheet->disconnectWorksheets();
	unset($spreadsheet);

  return $res;
}

function get_sheet_data ($rows_data, $sheet, $for_xml, $sheet_name = "")  {
	$first_row_headers = isset($rows_data["first_row_headers"]) && is_bool($rows_data["first_row_headers"]) ? $rows_data["first_row_headers"] : true;
	$first_row_number = isset($rows_data["first_row_number"]) && is_numeric($rows_data["first_row_number"]) ? $rows_data["first_row_number"] : 1;
	
	$highest_col = $sheet->getHighestColumn();
	$highest_row = $sheet->getHighestRow();
	
	if($first_row_headers) {
		
		$range_headers = "A" . $first_row_number . ":" . $highest_col . $first_row_number;
		$sheet_headers = $sheet->rangeToArray($range_headers, null, true, true, false);
		$sheet_headers = format_headers($sheet_headers[0], $for_xml, $sheet_name);
		$first_data_row_number = $first_row_number + 1;
		$range_data = "A" . $first_data_row_number . ":" . $highest_col . $highest_row;
		
	} else {
		$range_data = "A" . $first_row_number . ":" . $highest_col . $highest_row;
	}
	
	$sheet_data = $sheet->rangeToArray($range_data, "", true, true, false);
	$final_data = [];
	
	if($first_row_headers && count($sheet_headers)) {
		
		foreach($sheet_data as $row) {
			$new_row = [];
			foreach($row as $key => $cell) {
				$new_row[$sheet_headers[$key]] = $cell;
			}
			array_push($final_data, $new_row);
		}
		
	} else {
		$final_data = $sheet_data;
	}
	
	return [
		'sheet' => $sheet_name,
		'data' => $final_data
	];
}

function format_headers ($headers, $for_xml = false, $sheet_name = "") {
	$isEmpty = true;// check if headers array is totally empty
	$headers_new = [];
	
	foreach($headers as $key => $cell) {
		if($headers[$key] === null || $headers[$key] === "") {
			$headers_new[$key] = $for_xml ? "_" . $key : $key;
		} else {
			$isEmpty = false;
			$header = $for_xml ? validate_xml_name($cell) : $cell;
			
			if(count_elements_in_array($headers, $header) > 1 || count_elements_in_array($headers_new, $header) > 1) {
				$headers_new[$key] = $header . "_" . $key;
			} else {
				$headers_new[$key] = $header;
			}
		}
	}
	
	if($for_xml) {
		return $headers_new;
	} else {
		return $isEmpty ? [] : $headers_new;
	}	
}

function count_elements_in_array ($arr, $elem) {
	$count = 0;
	foreach($arr as $item) {
		if($item === $elem) {
			$count++;
		}
	}
	
	return $count;
}

function unlink_files ($files, $directory = null) {
	$directory = $directory ? $directory : get_upload_directory();
	foreach($files as $file) {
		unlink($directory . $file["file"]);
	}
}

function get_files ($directory = null) {
	$target_dir = $directory ? $directory : get_upload_directory();
	$file = $_FILES['fileToUpload'];
	
	if(is_array($file['name'])) { // only single files are allowed!
		array_push($files_names, [ "file" => '', "status" => false ]);
		return $files_names;
	}

	$file_name = basename($file["name"]);
	
	if(!check_files_mime_and_ext($file_name, $file["tmp_name"])) {
		unlink($file['tmp_name']);
		
		return [ "file" => $file_name, "status" => false ];
	}
	
	$file_ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
	$new_file_name = generateRandomString(20) . '.' . $file_ext . ".xyz123";
	$target_file = $target_dir . $new_file_name;
	
	if (move_uploaded_file($file["tmp_name"], $target_file)) {
		return [ "file" => $new_file_name, "status" => true ];
	} else {
		unlink($file['tmp_name']);
		return [ "file" => $new_file_name, "status" => false ];
	}
	
	return [ "file" => '', "status" => false ];// if no of above ifs would be entered, then return this value
}

function check_files_mime_and_ext ($file_name, $file_tmp_name) {
	$mime = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $file_tmp_name);
	$fileExt = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
	
	if((($fileExt === "csv") || ($fileExt === "tsv")) && ($mime === "text/plain")) {
		return true;
	} else if (($fileExt === "xlsx") && ($mime === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
		return true;
	} else if (($fileExt === "ods") && ($mime === "application/vnd.oasis.opendocument.spreadsheet")) {
		return true;
	} else if (($fileExt === "xml") && ($mime === "application/xml")) {
		return true;
	} else if (($fileExt === "xls") && ($mime === "application/vnd.ms-excel")) {
		return true;
	} else {
		false;
	}
	
	/*
		csv: text/plain
		csv dos: text/plain
		csv mac: text/plain
		tsv: text/plan

		xlsx: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
		xlsx (xml-like): application/vnd.openxmlformats-officedocument.spreadsheetml.sheet

		ods: application/vnd.oasis.opendocument.spreadsheet

		excel xml: application/xml

		xls 5.0-95: application/vnd.ms-excel
		xls 97-2003: application/vnd.ms-excel
	*/
}


?>